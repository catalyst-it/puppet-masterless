# puppet-masterless

This is a small tool designed to help you use Puppet *without* using a
Puppet Master. It packages a Puppet project as a self-contained shell
script and optionally applies it to a remote machine via SSH. You can
think of it like a remote version of the `puppet-apply` command[^1].

[^1]: In fact, running `puppet-apply` is exactly what it does in the end.

## Installation

### From RubyGems.org

To install from [RubyGems.org][] just run:

    gem install puppet-masterless

[RubyGems.org]: https://rubygems.org/gems/puppet-masterless

### From Source

You can use [Rake][] to build and install the program as a Gem:

    rake install:local

Alternatively, because the program is a single Ruby file, you can simply
put it somewhere on your PATH and make sure it's executable, along with
the man page.

    install -m 755 src/puppet-masterless /usr/local/bin/puppet-masterless
    install -m 644 man/puppet-masterless.1 /usr/local/share/man/man1/puppet-masterless.1

You can also bundle the man page with the executable and install both as
a single file:

    rake build:prepare
    install -m 755 bin/puppet-masterless /usr/local/bin/puppet-masterless

[rake]: https://ruby.github.io/rake/

## Usage

### Quick Start

To package a project, use the `package` command:

    puppet-masterless package confdir <confdir>

To apply a project to a remote host, use the `apply` command:

    puppet-masterless apply confdir <confdir> to <hostname>

In both situations, `<confdir>` should be the base directory of your
Puppet project. Refer to [Puppet's documentation][confdir] for details
about how this directory should be organised.

[confdir]: https://puppet.com/docs/puppet/5.5/dirs_confdir.html

### Options

The program accepts arguments according to a very simple command line
grammar. To see a summary of the available options and what they
control, run `puppet-masterless` without any arguments. For full
documentation, refer to its [man page][manual][^2] or help text
(accessed via `puppet-masterless help`).

[^2]: You can view a Gem's man page by installing the [gem-man][]
  extension and running `gem man <gem>`.

[gem-man]: https://rubygems.org/gems/gem-man

### Project Layout

It's assumed that your Puppet project is laid out something like this:

    <confdir>
    ├── puppet.conf
    ├── hiera.yaml
    ├── data
    │   └── ...
    ├── manifests
    │   └── ...
    └── modules
        └── ...

All of these paths can be overridden by command line arguments, but the
tool will work best if you use a project structure similar to this one.

Most importantly, all paths in your Hiera configuration should be
relative to the project's root, so that `puppet-apply` will only consult
Hiera data that's included in the package. In Hiera 5 this is usually
the case by default, since paths are considered relative to your
`hiera.yaml`:

    ---
    version: 5
    defaults:
      datadir: data
      data_hash: yaml_data
    hierarchy:
      - name: Common data
        path: common.yaml

In Hiera 3, this must be done manually. The following `hiera.yaml`
illustrates one way to do this, by explicitly setting `:datadir` to a
path under your project's `confdir`:

    ---
    :backends:
      - yaml
    :yaml:
      :datadir: "%{settings::confdir}/data"

## Resources

 - [Man page][manual]
 - [Source repository][repo]
 - [Issue tracker][issues]

[manual]: https://catalyst-it.gitlab.io/devtools/puppet-masterless/
[repo]: https://gitlab.com/catalyst-it/devtools/puppet-masterless/
[issues]: https://gitlab.com/catalyst-it/devtools/puppet-masterless/issues

## Contributing

 1. Fork it from <https://gitlab.com/catalyst-it/devtools/puppet-masterless>
 2. Create a feature branch (`git checkout -b my-new-feature`)
 3. Commit your changes (`git commit -am 'Add some feature'`)
 4. Push to the branch (`git push origin my-new-feature`)
 5. Create a Merge Request at <https://gitlab.com/catalyst-it/devtools/puppet-masterless/merge_requests>
