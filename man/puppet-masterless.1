.Dd October 10, 2019
.Dt PUPPET-MASTERLESS 1
.Os
.Sh NAME
.Nm puppet-masterless
.Nd apply a local puppet project to a remote host
.Sh SYNOPSIS
.Nm
.Ar command
.Ar [ argument ... ]
.No [ --
.Ar puppet-apply argument ...
.No ]
.Sh DESCRIPTION
The
.Nm
program packages a Puppet project as a self-contained shell script and
optionally executes it on a remote machine via SSH.
.Pp
Some simple conventions are used to decide what files to include in the
package. For a list of files that will be automatically included, refer
to
.Sx FILES .
.Sh COMMANDS
.Bl -tag -width Ds
.It Cm package
Package a project as a self-contained shell script.
.It Cm apply
Apply a project to a remote host, or to the local machine if no hostname
is specified with the
.Cm to
argument.
.El
.Sh ARGUMENTS
.Bl -tag -width Ds
.It Cm confdir Aq pathname
Set the root directory of the Puppet project. This value is passed to
.Fl -confdir
when running
.Xr puppet-apply 8 .
.Pp
This argument is required.
.It Cm manifest Aq filename
Specify the main Puppet manifest to apply. This may be a file or
directory.
.Pp
Defaults to
.Dq Ao confdir Ac Ns /manifests
(if that file is present).
.It Cm hiera_config Aq filename
Specify the Hiera configuration file to use. This value is passed to
.Fl -hiera_config
when running
.Xr puppet-apply 8 .
.Pp
Defaults to
.Dq Ao confdir Ac Ns /hiera.yaml
(if that file is present).
.It Cm modulepath Aq pathname
Set the Puppet modules directory. This value is passed to
.Fl -modulepath
when running
.Xr puppet-apply 8 .
.Pp
Defaults to
.Dq Ao confdir Ac Ns /modules
(if that file is present).
.It Cm file Aq filename
Add an arbitrary file or directory to the generated package. This can be
used to include files in non-standard locations.
.Pp
Directories are added recursively, and this option can be repeated to
include multiple files.
.It Cm puppet Aq executable
.Pp
Specify the location of the
.Xr puppet 8
executable on the remote host. This is useful when the puppet binary is
installed in a directory that is not on the active user's
.Ev PATH
(for example under
.Pa /opt/puppetlabs/bin Ns ).
.Pp
Defaults to
.Dq puppet .
.It Cm output file Aq filename
Set the output filename. If not given, a randomly-generated filename of the form
.Dq puppet-package-XXXXXXXX
is used.
.Pp
Applies only to the
.Cm package
command.
.It Cm sudo Aq executable
Specify the location of the
.Xr sudo 1
executable on the remote host. This can also be used to specify an
alternative command for privilege elevation, for example
.Xr doas 1 .
.Pp
Applies only to the
.Cm apply
command.
.Pp
Defaults to
.Dq sudo .
.It Cm as Aq username
Set the name of the user that will run
.Xr puppet-apply 8
on the remote host.
.Xr sudo 1
is used to become this user before execution.
.Pp
Applies only to the
.Cm apply
command.
.Pp
Defaults to
.Dq root .
.It Cm to Aq hostname
Specify the remote host to provision. If not given, the project will be
applied directly to the local machine (without packaging or SSH).
.Pp
Applies only to the
.Cm apply
command.
.It Cm and, with
These words are ignored. They can be used to make an invocation of the
program read sensibly (or insensibly, as desired).
.It Cm -- [ Ar argument ... ]
Two dashes indicate the end of program arguments. Any remaining
arguments are passed to
.Xr puppet-apply 8
verbatim.
.\" empty par for -T html spacing
.Pp
\&
.El
For more information about the
.Cm confdir ,
.Cm hiera_config ,
.Cm manifest , No and
.Cm modulepath No settings, refer to the following Puppet documentation:
.Pp
.Bl -dash -compact
.It
.Lk https://puppet.com/docs/puppet/5.5/dirs_confdir.html confdir
.It
.Lk https://puppet.com/docs/puppet/5.5/hiera_config_yaml_5.html hiera_config
.It
.Lk https://puppet.com/docs/puppet/5.5/dirs_manifest.html manifest
.It
.Lk https://puppet.com/docs/puppet/5.5/dirs_modulepath.html modulepath
.El
.Sh FILES
The following files are automatically included in the package, when
present:
.\" empty par for -T html spacing
.Pp
\&
.Bl -column "                  "
.It Ao confdir Ac Ns /puppet.conf Ta - Default Puppet configuration file.
.It Ao confdir Ac Ns /hiera.yaml  Ta - Default hiera configuration file.
.It Ao confdir Ac Ns /data        Ta - Default hiera data directory.
.It Ao confdir Ac Ns /manifests   Ta - Default manifest path.
.It Ao confdir Ac Ns /modules     Ta - Default module path.
.El
.Pp
Directories are included recursively.
.Sh EXIT STATUS
.Ex -std
.Sh EXAMPLES
Apply the Puppet project in
.Pa ./puppet
directly to the local machine:
.Bd -literal -offset indent
puppet-masterless apply confdir puppet
.Ed
.Pp
Apply the
.Pa site.pp
manifest from the project in
.Pa ./config
to host 10.0.8.1 as user
.Dq charlize :
.Bd -literal -offset indent
puppet-masterless apply confdir config \\
  with manifest config/manifests/site.pp \\
  to 10.0.8.1 as charlize
.Ed
.Pp
Bundle the
.Pa test
project as a shell script, including a non-standard Hiera configuration
file location and data directory:
.Bd -literal -offset indent
puppet-masterless package confdir test \\
  with file hiera and hiera_config hiera/test.yaml \\
  output file puppet.sh
.Ed
.Pp
.Sh CAVEATS
For this program to be useful, Puppet must be installed on the remote
host.
.Pp
All file paths must be relative to the current working directory. An
error is signaled whenever an absolute path is specified.
.Pp
This program is only intended to work with Puppet 4.10 and 5.5. It may
work with other versions of Puppet, or it may not.
.Pp
The SSH functionality of this program is intentionally simplistic. If
you need more complex behaviour when connecting to remote machines, use
the
.Cm package
command in combination with a tool like
.Xr ansible 1 ,
.Xr cap 1 or
.Xr fab 1 .
.Pp
Very little information is displayed when an error occurs.
.Sh SECURITY CONSIDERATIONS
The
.Cm apply
command uses
.Xr sudo 1
to assume a given user's identity on the remote host, with all of the
security issues that implies.
.Pp
When applying manifests to a remote machine, any secrets included in the
project will be present on the filesystem for as long as it takes to run
.Xr puppet-apply 8 .
.Sh IMPLEMENTATION NOTES
.Nm
is a single Ruby script with no Gem dependencies. It should be possible
to copy and run on any machine with Ruby 1.9 or newer.
.Sh SEE ALSO
.Xr puppet 8 ,
.Xr puppet-apply 8 ,
.Xr puppet-master 8
.Sh AUTHORS
.An Evan Hanson Aq Mt evanh@catalyst.net.nz
