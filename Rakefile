# -*- mode: ruby -*-
# vi: set ft=ruby :
#
# Copyright (c) 2017-2018 Catalyst.net Ltd
#
# This file is part of puppet-masterless.
#
# puppet-masterless is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# puppet-masterless is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with puppet-masterless. If not, see http://www.gnu.org/licenses/
#

require 'bundler/gem_tasks'
require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new(:spec)

task default: :spec

gem = Bundler::GemHelper.gemspec
deb = "#{gem.name}_#{gem.version}_all.deb"

begin
  load 'Rakefile.local'
rescue LoadError
  # no local tasks to load
end

Rake::Task[:build].enhance [:'build:prepare']

namespace :build do
  desc "Prepare all files for a build"
  task :prepare do
    sh "rm -fr bin"
    sh "mkdir -p bin"
    sh "sed 's/VERSION = .*/VERSION = %{#{gem.version}}/1' < src/#{gem.name} > bin/#{gem.name}"
    sh "chmod 755 bin/#{gem.name}"
    sh "echo >> bin/#{gem.name}"
    sh "sed 's/^/#1 /g' < man/#{gem.name}.1 >> bin/#{gem.name}"
  end

  begin
    require 'fpm'
    desc "Build #{deb} into the pkg directory"
    task :deb => [:prepare] do
      sh "rm -fr build"
      sh "mkdir -p build/bin"
      sh "mkdir -p build/share/man/man1"
      sh "mkdir -p pkg"
      sh "cp -R bin/#{gem.name} build/bin/#{gem.name}"
      sh "cp -R man/#{gem.name}.1 build/share/man/man1/#{gem.name}.1"
      sh "gzip build/share/man/man1/#{gem.name}.1"
      sh "fpm --force \
              --input-type dir \
              --output-type deb \
              --architecture all \
              --package pkg/#{deb} \
              --name '#{gem.name}' \
              --version '#{gem.version}' \
              --description '#{gem.description}' \
              --vendor '#{gem.author} <#{gem.email}>' \
              --maintainer '#{gem.author} <#{gem.email}>' \
              --license '#{gem.license}' \
              --url '#{gem.homepage}' \
              --depends 'ruby >= 1.9' \
              --deb-no-default-config-files \
              --prefix /usr \
              --chdir build ."
    end
  rescue LoadError
    # no fpm, skip deb task
  end
end
